$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
		//CARROSSEL DE DESTAQUE
		$("#carrosselPortal").owlCarousel({
			items : 3,
			dots: false,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : false,

			//CARROSSEL RESPONSIVO
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
				},
				600:{
					items:1
				},
				991:{
					items:2,
				},
				1200:{
					items:3
				},

			}

		});
		//BOTÕES DO CARROSSEL DE PRODUTOS RELACIONADOS
		var carrosselPortal = $("#carrosselPortal").data('owlCarousel');
		$('.esquerdaCarrossel').click(function(){ carrosselPortal.prev(); });
		$('.direitaCarrossel').click(function(){ carrosselPortal.next(); });

	var userFeed = new Instafeed({
		get: 'user',
		userId: '431966971',
		clientId: 'c7c6b34f76c04fd695e16fd6b5769c4f',
		accessToken: '431966971.1677ed0.41fb392cdc854aa3b717c612f19e0a46',
		resolution: 'standard_resolution',
		template: '<a href="{{link}}" class="item" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes"><i class="fas fa-heart"></i> {{likes}}</span><span class="comments"> <i class="fas fa-comment"></i> {{comments}}</span></small></div></a>',
		sortBy: 'most-recent',
		limit: 6,
		links: false
	});

	userFeed.run();

});
